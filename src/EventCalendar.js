// @flow
import {
  VirtualizedList,
  View,
  TouchableOpacity,
  Image,
  Text
} from 'react-native'
import _ from 'lodash'
import moment from 'moment'
import React from 'react'

import styleConstructor from './style'

import DayView from './DayView'

export default class EventCalendar extends React.Component {
  constructor(props) {
    super(props)
    this.styles = styleConstructor(props.styles)
    this.state = {
      date: moment(this.props.initDate),
      index: this.props.size
    }
  }

  static defaultProps = {
    size: 1,
    initDate: new Date(),
    formatHeader: 'DD MMMM YYYY'
  }

  _getItemLayout(data, index) {
    const { width } = this.props
    return { length: width, offset: width * index, index }
  };

  _getItem(events, index) {
    return events;
  }

  _renderItem({ index, item }) {
    const { width, format24h, initDate, scrollToFirst, startHour, endHour } = this.props
    const date = moment(initDate).add(index - this.props.size, 'days')
    return (
      <DayView
        date={date}
        index={index}
        format24h={format24h}
        formatHeader={this.props.formatHeader}
        headerStyle={this.props.headerStyle}
        renderEvent={this.props.renderEvent}
        eventTapped={this.props.eventTapped}
        events={item}
        width={width}
        styles={this.styles}
        scrollToFirst={scrollToFirst}
        startHour={startHour}
        endHour={endHour}
        initDate={initDate}
      />
    )
  }

  render() {
    const {
      width,
      virtualizedListProps,
      events,
      initDate,
      formatHeader,
      onDateChanged,
    } = this.props

    return (
      <View style={[this.styles.container, { width }]}>
        <VirtualizedList
          ref='calendar'
          windowSize={1}
          initialNumToRender={1}
          initialScrollIndex={0}
          data={events}
          getItemCount={() => this.props.size}
          getItem={this._getItem.bind(this)}
          keyExtractor={(item, index) => index}
          getItemLayout={this._getItemLayout.bind(this)}
          horizontal
          renderItem={this._renderItem.bind(this)}
          style={{ width: width }}
          onMomentumScrollEnd={() => {}}
          {...virtualizedListProps}
        />
      </View>
    )
  }
}
