// @flow
import { View, Text, ScrollView, TouchableOpacity } from "react-native";
import populateEvents from "./Packer";
import React, { Fragment } from "react";
import moment from "moment";
import _ from "lodash";

const LEFT_MARGIN = 60 - 1;
// const RIGHT_MARGIN = 10
const CALENDAR_HOUR_HEIGHT = 100;
// const EVENT_TITLE_HEIGHT = 15
const TEXT_LINE_HEIGHT = 17;
// const MIN_EVENT_TITLE_WIDTH = 20
// const EVENT_PADDING_LEFT = 4
const CALENDAR_MARGIN = 25;
const EVENT_MARGIN = 2;

export default class DayView extends React.PureComponent {
  constructor(props) {
    super(props);
    const width = props.width - LEFT_MARGIN;
    const packedEvents = populateEvents(props.events, width);
    let initPosition = _.min(_.map(packedEvents, "top")) - CALENDAR_HOUR_HEIGHT;
    initPosition = initPosition < 0 ? 0 : initPosition;
    this.state = {
      _scrollY: initPosition,
      packedEvents,
    };
  }

  componentWillReceiveProps(nextProps) {
    const width = nextProps.width - LEFT_MARGIN;
    this.setState({
      packedEvents: populateEvents(nextProps.events, width),
    });
  }

  componentDidMount() {
    this.props.scrollToFirst && this.scrollToFirst();
  }

  scrollToFirst() {
    setTimeout(() => {
      if (this.state && this.state._scrollY && this._scrollView) {
        this._scrollView.scrollTo({ x: 0, y: this.state._scrollY, animated: true });
      }
    }, 1);
  }

  _renderRedLine() {
    const offset = CALENDAR_HOUR_HEIGHT;
    const { width, styles, format24h, startHour, initDate } = this.props;
    const timeNowHour = moment().hour();
    const timeNowMin = moment().minutes();
    const sameDate = moment().isSame(moment(initDate), "days");
    return sameDate ? (
      <View
        key={`timeNow`}
        style={[
          styles.lineNow,
          {
            top: offset * (timeNowHour - startHour) + (offset * timeNowMin) / 60 + CALENDAR_MARGIN,
            width: width - 20,
          },
        ]}
      />
    ) : (
      <View />
    );
  }

  _renderLines() {
    const offset = CALENDAR_HOUR_HEIGHT;
    const { format24h, startHour, endHour } = this.props;
    const start = startHour || 0;
    const end = endHour + 1 || 25;

    return _.range(start, end).map((t, i) => {
      let timeText;
      if (t === 0) {
        timeText = ``;
      } else if (t < 12) {
        timeText = !format24h ? `${t}:00 AM` : `${t}:00`;
      } else if (t === 12) {
        timeText = !format24h ? `${t}:00 PM` : `${t}:00`;
      } else if (t === 24) {
        timeText = !format24h ? `12:00 AM` : `0:00`;
      } else {
        timeText = !format24h ? `${t - 12}:00 PM` : `${t}:00`;
      }
      let timeText30;
      if (t === 0) {
        timeText30 = ``;
      } else if (t < 12) {
        timeText30 = !format24h ? `${t}:30 AM` : `${t}:30`;
      } else if (t === 12) {
        timeText30 = !format24h ? `${t}:30 PM` : `${t}:30`;
      } else if (t === 24) {
        timeText30 = !format24h ? `12:30 AM` : `0:30`;
      } else {
        timeText30 = !format24h ? `${t - 12}:30 PM` : `${t}:30`;
      }

      const { width, styles } = this.props;
      return [
        <Text key={`timeLabel${i}`} style={[styles.timeLabel, { top: offset * i - 6 + CALENDAR_MARGIN }]}>
          {timeText}
        </Text>,
        t === 0 ? null : <View key={`line${i}`} style={[styles.line, { top: offset * i + CALENDAR_MARGIN, width: width - 20 }]} />,
        t !== end - 1 && (
          <View key={`half-${i}`}>
            <Text key={`timeLabel${i}-30`} style={[styles.timeLabel, { top: offset * (i + 0.5) - 6 + CALENDAR_MARGIN }]}>
              {timeText30}
            </Text>
            <View key={`lineHalf${i}`} style={[styles.line, { top: offset * (i + 0.5) + CALENDAR_MARGIN, width: width - 20 }]} />
          </View>
        ),
      ];
    });
  }

  _onEventTapped(event) {
    this.props.eventTapped(event);
  }

  _renderEvents() {
    const { styles, startHour } = this.props;
    const { packedEvents } = this.state;
    let events = packedEvents.map((event, i) => {
      const style = {
        left: event.left,
        height: event.height - 2 * EVENT_MARGIN,
        width: event.width,
        top: event.top + EVENT_MARGIN,
      };
      style.top = style.top - startHour * CALENDAR_HOUR_HEIGHT + CALENDAR_MARGIN;

      // Fixing the number of lines for the event title makes this calculation easier.
      // However it would make sense to overflow the title to a new line if needed
      const numberOfLines = Math.floor(event.height / TEXT_LINE_HEIGHT);
      const formatTime = this.props.format24h ? "HH:mm" : "hh:mm A";
      const TitleComponent = (
        <Text numberOfLines={1} style={styles.eventTitle}>
          {event.title || "Event"}
        </Text>
      );

      return (
        <Fragment>
          {this.props.renderEvent ? (
            <TouchableOpacity key={i} style={[styles.event, style, { backgroundColor: event.color }]} activeOpacity={1.0} onPress={() => this._onEventTapped(this.props.events[event.index])}>
              {this.props.renderEvent(event)}
            </TouchableOpacity>
          ) : (
            <TouchableOpacity key={i} style={[styles.event, style, { backgroundColor: event.color }]} activeOpacity={1.0} onPress={() => this._onEventTapped(this.props.events[event.index])}>
              {event.titleIcon ? (
                <View style={styles.titleIconWrapper}>
                  {event.titleIcon}
                  {TitleComponent}
                </View>
              ) : (
                <Fragment>{TitleComponent}</Fragment>
              )}

              {numberOfLines > 1 && event.summary ? (
                <Text numberOfLines={numberOfLines - 1} style={[styles.eventSummary]}>
                  {event.summary || " "}
                </Text>
              ) : null}
              {numberOfLines > 2 || (numberOfLines > 1 && !event.summary) ? (
                <Text style={styles.eventTimes} numberOfLines={1}>
                  {moment(event.start).format(formatTime)} - {moment(event.end).format(formatTime)}
                </Text>
              ) : null}
            </TouchableOpacity>
          )}
        </Fragment>
      );
    });

    return (
      <View>
        <View style={{ marginLeft: LEFT_MARGIN }}>{events}</View>
      </View>
    );
  }

  render() {
    const { styles, startHour, endHour } = this.props;
    return (
      <ScrollView
        ref={(ref) => (this._scrollView = ref)}
        contentContainerStyle={[
          styles.contentStyle,
          {
            width: this.props.width,
            height: (endHour - startHour) * CALENDAR_HOUR_HEIGHT + 2 * CALENDAR_MARGIN,
          },
        ]}
      >
        {this._renderLines()}
        {this._renderEvents()}
        {this._renderRedLine()}
      </ScrollView>
    );
  }
}
