# React Native Event Calendar
A React Native iOS style calendar implemented using VirtualizedList.
This package is forked of https://github.com/joshyhargreaves/react-native-event-calendar
allow run without expo and improve somethings

## Demo
<img src="https://raw.githubusercontent.com/duyluonglc/react-native-events-calendar/master/demo/screenshot.png" width="360">

## Current API
Property | Type | Description
------------ | ------------- | -------------
events | PropTypes.array | Array of event
width | PropTypes.number | Container width
format24h | PropTypes.boolean | Use format 24hour or 12hour
formatHeader | PropTypes.string | Header date format
headerStyle | PropTypes.object | Header style
renderEvent | PropTypes.function | Function return a component to render event `renderEvent={(event) => <Text>{event.title}</Text>}`
eventTapped | PropTypes.function | Function on event press
initDate | PropTypes.string | show initial date (default is today)
scrollToFirst | PropTypes.boolean | scroll to first event of the day (default true)
size | PropTypes.number | number of date will render before and after initDate (default is 30 will render 30 day before initDate and 29 day after initDate)
virtualizedListProps | PropTypes.object | prop pass to virtualizedList
styles | PropTypes.object | styles for calendar
startHour | PropTypes.number | calendar start hour
endHour | PropTypes.number | calendar end hour

`EventCalendar` can be configured through a `style` prop whereby any of the components in the calendar can be styled. 
```
    {
        container: {
            backgroundColor: 'white'
        }, 
        event: {
            opacity: 0.5
        }
    }
```
## Install
`npm i --save git@bitbucket.org:ordergroup/og-react-native-event-calendar.git`

## Examples
See Examples dir. 

```js
import EventCalendar from 'react-native-events-calendar'

let { width } = Dimensions.get('window')

const events = [
    { start: '2020-02-20 08:30:00', end: '2020-02-20 10:30:00', title: 'Order Group', summary: 'Enjoy!', color: '#ffffff', id: '1'},
    { start: '2020-02-20 09:30:00', end: '2020-02-20 12:20:00', title: 'Is the best', summary: 'Boom!', color: '#000000', id: '2' },
]


render () {
  return (
    <EventCalendar
      eventTapped={this._eventTapped.bind(this)}
      events={this.state.events}
      width={width}
      initDate={'2020-02-20'}
    />
  )
}

```
